#include <iostream>

using namespace std;

template<typename type> class TComplex{
	type im;
	type re;
public:
	TComplex(type re = type(), type im = type()) : re(re),im(im) { }
	TComplex(const TComplex& obiekt){
		this->im = obiekt.im;
		this->re = obiekt.re;
	}
	void setRe(const type re){
		this->re = re;
	}

	void setIm(const type im){
		this->im = im;
	}

	const type getRe() const {
		return re;
	}

	const type getIm() const {
		return im;
	}

	const TComplex& operator=(const TComplex& obiekt){
		this->im = obiekt.im;
		this->re = obiekt.re;
			return *this;
	}

	const void operator+(const TComplex& obiekt){
		this->im += obiekt.im;
		this->re += obiekt.re;
	}

	const void operator-(const TComplex& obiekt){
		this->im -= obiekt.im;
		this->re -= obiekt.re;
	}

	const void operator+=(const TComplex& obiekt){
		this->im += obiekt.im;
		this->re += obiekt.re;
	}

	const void operator-=(const TComplex& obiekt){
		this->im -= obiekt.im;
		this->re -= obiekt.re;
	}

	const void operator+(){
		this->re = +this->re;
		this->im = +this->im;
	}

	const void operator-(){
		this->re = -this->re;
		this->im = -this->im;
	}

    const TComplex& operator*(const TComplex& comp){
        TComplex *temp = new TComplex;
        temp->re=(re*comp.re)+(im*comp.im);
        temp->im=(re*comp.im)+(im*comp.re);
        return *temp;
    }



	friend ostream& operator<<(ostream& out,const TComplex<type>& obiekt){
		out << "Re = " << obiekt.re << " Im =" << obiekt.im;
		return out;
	}

	friend istream& operator>>(istream& in,TComplex<type>& obiekt){
		in >> obiekt.re >> obiekt.im;
		return in;
	}

	virtual ~TComplex() { }	
};

int main(){
	TComplex<double> liczba(1,-3);

	cout << liczba.getIm();

	return 0;
}